#include <iostream>
#include "thread_pool.h"
#include <future>

using namespace std;

void hello()
{
    cout << "Hello form function" << endl;
}

int answer()
{
    return 42;
}

int main()
{
    thread_pool tp(4);
    for (int i = 0 ; i < 100 ; ++i)
        tp.add_task( [i] {
            this_thread::sleep_for(10ms);
            cout << "Hello from lambda " << i << endl;
            } );
    tp.add_task( hello );
    future<int> res = tp.async(answer);
    cout << res.get() << endl;
    return 0;
}

