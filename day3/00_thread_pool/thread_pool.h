#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include <thread>
#include "../../day2/01_thread_safe_queue/thread_safe_queue.h"
#include <vector>
#include <functional>
#include <future>

//typedef std::function<void()> task_t;
using task_t = std::function<void()>;

class thread_pool
{
    thread_safe_queue<task_t> q;
    std::vector<std::thread> pool;

    void worker()
    {
        for(;;)
        {
            task_t task;
            q.pop(task);
            if (!task)
                return;
            task();
            //q.task_done();
        }
    }

public:
    thread_pool(size_t size) : q(64)
    {
        for (int i = 0 ; i < size ; ++i)
        {
            pool.emplace_back(&thread_pool::worker, this);
            //std::thread(&thread_pool::worker, this).detach();
        }
    }

    void add_task(task_t task)
    {
        if (task)
            q.push(task);
    }

    template <typename F>
    auto async(F f) -> std::future<decltype(f())>
    {
        auto task = std::make_shared<std::packaged_task<decltype(f())()>>(f);
        auto fres = task->get_future();
        q.push([task] { (*task)(); });
        return fres;
    }

    ~thread_pool()
    {
        // join pool
        for(auto& th : pool) q.push(nullptr);
        for(auto& th : pool) th.join();
        //q.join();
    }
};

#endif // THREAD_POOL_H
