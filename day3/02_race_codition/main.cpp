#include <iostream>
#include <thread>
#include <vector>
#include <atomic>
#include <mutex>

using namespace std;


long counter = 0;

class spinlock_mutex
{
    std::atomic_flag flag;
public:
    spinlock_mutex() : flag(ATOMIC_FLAG_INIT)
    {
    }

    void lock()
    {
        while(flag.test_and_set());
    }

    void unlock()
    {
        flag.clear();
    }

};

spinlock_mutex mtx;

void increase()
{
    for (int i = 0 ; i < 100000 ; ++i)
    {
        lock_guard<spinlock_mutex> l(mtx);
        ++counter;
        //if (counter == 1000) return;
    }
}

int main()
{
    cout << "Race Condition" << endl;
    //cout << "Is this lock-free? " << counter.is_lock_free() << endl;
    vector<thread> thds;

    auto start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 2 ; ++i)
        thds.emplace_back(increase);
    for (auto& th : thds) th.join();

    auto end = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count() << " us" << endl;

    cout << "Counter = " << counter << endl;
    return 0;
}

