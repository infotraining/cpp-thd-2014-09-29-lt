#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>
#include <string>
#include <mutex>

#include "active_object.h"

using namespace std;


class Logger
{
    ofstream fout_;
    active_object ao;

public:
    Logger(const string& file_name)
    {
        fout_.open(file_name);
    }

    ~Logger()
    {
        ao.send( [this] { fout_.close(); });
    }

    void log(const string& message)
    {
        ao.send( [this, message] {
            fout_ << message << endl;
            fout_.flush();
            //this_thread::sleep_for(10ms);
        } );

    }
};

void run(Logger& logger, int id)
{
    for(int i = 0; i < 1000; ++i)
        logger.log("Log#" + to_string(id) + " - Event#" + to_string(i));
}

int main()
{
    /*
     * Napisz klase Logger, ktora jest thread-safe
     */

    Logger log("data.log");

    auto start = chrono::high_resolution_clock::now();
    thread thd1(&run, ref(log), 1);
    thread thd2(&run, ref(log), 2);

    thd1.join();
    thd2.join();

    auto end = chrono::high_resolution_clock::now();
    cout << "Time: " << chrono::duration_cast<chrono::microseconds>(end-start).count() << " us" << endl;
}
