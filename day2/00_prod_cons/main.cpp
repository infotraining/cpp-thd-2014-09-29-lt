#include <iostream>
#include <thread>
#include <vector>
#include <queue>
#include <mutex>
#include <condition_variable>

#include <iostream>

using namespace std;

queue<int> q;
mutex q_mtx;
condition_variable cond;

void producer()
{
    for (int i = 0 ; i < 10000 ; ++i)
    {
        {
            lock_guard<mutex> l(q_mtx);
            q.push(i);
            cond.notify_one();
        }
        this_thread::sleep_for(10ms);
    }
}

void consumer(int id)
{
    for(;;)
    {
        unique_lock<mutex> l(q_mtx);

//        while(q.empty())
//        {
//            cond.wait(l);
//            /* l.unlock()
//             * wait...
//             * l.lock() */
//        }
        cond.wait(l, [] () { return !q.empty(); });

        int msg = q.front();
        q.pop();
        cout << id << " just got " << msg << endl;

    }

}

int main()
{
    cout << "Hello Producer Consumer!" << endl;
    vector<thread> thds;
    thds.emplace_back(&producer);
    thds.emplace_back(&consumer, 1);
    thds.emplace_back(&consumer, 2);
    for (auto& th : thds) th.join();
    return 0;
}

