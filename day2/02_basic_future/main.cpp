#include <iostream>
#include <thread>
#include <future>

using namespace std;

int answer()
{
    this_thread::sleep_for(5s);
    return 42;
}

int main()
{
    cout << "Hello Future!" << endl;

    packaged_task<int()> pt(answer);
    future<int> res = pt.get_future();
    thread th1(move(pt));
    th1.detach();
    //std::future<int> res = async(launch::async, answer);
    cout << "After answer" << endl;

    cout << "Answer is: " << res.get() << endl;
    return 0;
}

