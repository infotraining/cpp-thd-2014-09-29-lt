#include <iostream>
#include <thread>
#include <vector>
#include <queue>
#include <mutex>
#include <condition_variable>

#include "thread_safe_queue.h"

using namespace std;

thread_safe_queue<long> q(1024);
mutex c_mtx;

bool is_prime(long n)
{
    for (long div = 2 ; div < n ; ++div)
    {
        if (n % div == 0)
            return false;
    }
    return true;
}

void producer()
{
    for (long i = 1 ; i < 100; ++i)
    {
        {
            q.push(i);
        }
        //this_thread::sleep_for(1ms);
    }
}

void consumer(int id)
{
    for(;;)
    {
        long msg;
        q.pop(msg);
        if (is_prime(msg))
        {
            lock_guard<mutex> l(c_mtx);
            cout << id << " just got " << msg << endl;
        }
        q.task_done();
    }
}

template<typename T>
class ProducerConsumer
{
    thread_safe_queue<T> q;
    vector<thread> producers;

public:
    ProducerConsumer() : q(100)
    {}

    ~ProducerConsumer()
    {
        cout << "Destructor begins" << endl;
        for(auto& th : producers) th.join();
        cout << "After joining producers" << endl;
        q.join();
        cout << "After joining q" << endl;
    }

    void add_producer(std::function<void(thread_safe_queue<T>&)> fprod)
    {
        producers.emplace_back(fprod, ref(q));
    }

    void add_consumer(std::function<void(T)> fcons, int n_of_consumers=1)
    {
        for (int i = 0 ; i < n_of_consumers ; ++i)
        {
            thread( [this, fcons] () {
                for(;;)
                {
                    T item;
                    q.pop(item);
                    fcons(item);
                    q.task_done();
                }
            } ).detach();
        }
    }

};

int main()
{
    cout << "Hello Producer Consumer!" << endl;
    //vector<thread> thds;
//    thread th_prod(&producer);
//    thread(consumer, 1).detach();
//    thread(consumer, 2).detach();
//    th_prod.join();
//    q.join();
    ProducerConsumer<int> pc;
    pc.add_producer( [] (thread_safe_queue<int>& q) {
        for (long i = 1 ; i < 100; ++i)
        {
            q.push(i);
            this_thread::sleep_for(1ms);

        }
    });
    pc.add_consumer( [] (auto msg) {
        if (is_prime(msg))
        {
            //lock_guard<mutex> l(c_mtx);
            cout << this_thread::get_id() <<" just got " << msg << endl;
        }
    }, 4);
    return 0;
}

