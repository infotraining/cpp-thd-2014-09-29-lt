#ifndef THREAD_SAFE_QUEUE_H
#define THREAD_SAFE_QUEUE_H

#include <queue>
#include <condition_variable>
#include <mutex>
#include <vector>
#include <atomic>
#include <iostream>
#include <thread>

template <typename T>
class thread_safe_queue
{
    std::mutex q_mtx;
    std::queue<T> q;
    std::condition_variable cond_out;
    std::condition_variable cond_in;
    size_t max_size;

    std::atomic<int> task_counter;

public:
    thread_safe_queue(size_t size) : max_size(size), task_counter(0)
    {

    }

    void push(T&& item)
    {
        std::unique_lock<std::mutex> l(q_mtx);
        while( q.size() >= max_size)
        {
            cond_in.wait(l);
        }
        q.push(std::move(item));
        cond_out.notify_one();        
    }

    void push(const T& item)
    {
        std::unique_lock<std::mutex> l(q_mtx);
        while( q.size() >= max_size)
        {
            cond_in.wait(l);
        }
        q.push(item);
        cond_out.notify_one();
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> l(q_mtx);
        cond_out.wait(l, [this] () { return !q.empty(); });
        item = std::move(q.front());
        q.pop();
        cond_in.notify_one();
        ++task_counter;
    }

    bool try_pop(T& item)
    {
        std::lock_guard<std::mutex> l(q_mtx);
        if (q.empty())
            return false;
        item = std::move(q.front());
        q.pop();
        cond_in.notify_one();
        ++task_counter;
        return true;
    }

    void join()
    {
        //std::cout << "counter... = " << task_counter << " qsize = " << q.size() << std::endl;
        for(;;)
        {
            {
                std::lock_guard<std::mutex> l(q_mtx);
                if(q.empty() && !task_counter)
                {
                    //cond_out.notify_all();
                    return;
                }
            }
            std::this_thread::yield();
        }

    }

    void task_done()
    {
        --task_counter;
    }
};

#endif // THREAD_SAFE_QUEUE_H
