#include <iostream>
#include <algorithm>
#include <future>
#include <vector>

using namespace std;

template <typename It, typename T>
T parallel_accumulate(It begin, It end, T init)
{
    int N = thread::hardware_concurrency();
    vector<future<T>> futures;

    size_t block_size = distance(begin, end)/N;

    It block_start = begin;
    for(int i = 0 ; i < N ; ++i)
    {
        It block_end = block_start;
        advance(block_end, block_size);
        if (i == N-1) block_end = end;

        futures.push_back( async(launch::async,
                                 accumulate<It, T>,
                                 block_start, block_end, T()));

        block_start = block_end;
    }

    //T res = 0;
    //for (auto& fut : futures) res += fut.get();
    return accumulate(futures.begin(), futures.end(), 0L,
                      [] (long x, future<T>& y) { return x + y.get();});
}

int main()
{
    vector<long> v;

    for(long i = 0 ; i < 100'000'000 ; ++i)
    {
        v.push_back(i);
    }

    auto time_start = chrono::high_resolution_clock::now();
    long res = accumulate(v.begin(), v.end(), 0L);
    auto time_end = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::microseconds>(time_end-time_start).count();
    cout << " um - serial" << endl;
    cout << "Res = " << res << endl;

    time_start = chrono::high_resolution_clock::now();
    res = parallel_accumulate(v.begin(), v.end(), 0L);
    time_end = chrono::high_resolution_clock::now();
    cout << chrono::duration_cast<chrono::microseconds>(time_end-time_start).count();
    cout << " um  - parallel" << endl;
    cout << "Res = " << res << endl;


    return 0;
}

