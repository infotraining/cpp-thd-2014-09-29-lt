#include <iostream>
#include <thread>
#include <vector>
#include <ostream>

/* https://bitbucket.org/infotraining/ */
/* leszek.tarkowski@gmail.com */

using namespace std;

void hello(int id)
{

    cout << "Hello from thread " << id << " id: " << this_thread::get_id() << endl;
    this_thread::sleep_for(10ms);
}

void print_vector(vector<int>& v)
{
    for (auto& el : v) cout << el << endl;
}

struct Functor
{
    void operator()(int id)
    {
        cout << "Hello from functor " << id << " id: " << this_thread::get_id() << endl;
        this_thread::yield();
    }
};

thread generate_thread()
{
    vector<int> elem {100,200,300,400,500};
    //thread tmp( &print_vector, ref(elem));  // refernce to local - error
    thread tmp( [elem] () mutable { print_vector(elem); });
    return tmp;
}

int main()
{
    cout << "How many cores? = " << thread::hardware_concurrency() << endl;
    vector<int> v {1,2,3,4,5,6};

    vector<thread> threads;
    //thread th1(&hello, 1);
    //threads.push_back(move(th1));
    //th1.join();
    Functor f;
    threads.emplace_back(&hello, 1);
    threads.emplace_back(&print_vector, ref(v));
    threads.emplace_back(f, 2);
    threads.emplace_back( [&v] () { cout << "Hello from lambda " << v[2] << endl;} );
    threads.push_back(generate_thread());

    this_thread::sleep_for(chrono::milliseconds(1));
    cout << "After launching thread" << endl;

    for (thread& th : threads) th.join();
    return 0;
}

