#include <iostream>
#include <thread>
#include <vector>
#include <mutex>

using namespace std;

class BankAccount
{
    int id;
    double balance;
    mutex mtx;

public:
    BankAccount(int id, double balance) : id(id), balance(balance)
    {}

    void print()
    {
        cout << "Account " << id << " balance " << balance << endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        unique_lock<mutex> l(mtx, defer_lock);
        unique_lock<mutex> lto(to.mtx, defer_lock);
        lock(l, lto);
        balance -= amount;
        to.balance += amount;
    }
};

void test_transfer(BankAccount& ba1, BankAccount& ba2)
{
    for (int i = 0; i < 100000 ; ++i)
    {
        ba1.transfer(ba2, 1.0);
    }
}

int main()
{
    cout << "Bank Accounts" << endl;
    BankAccount ba1(1, 200000);
    BankAccount ba2(2, 200000);
    thread th1(test_transfer, ref(ba1), ref(ba2));
    thread th2(test_transfer, ref(ba2), ref(ba1));
    th1.join();
    th2.join();
    ba1.print();
    ba2.print();
    return 0;
}


